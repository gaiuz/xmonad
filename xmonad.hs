-- Main
import XMonad hiding ( (|||) )
import System.IO

-- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat, isDialog)

-- Actions
import XMonad.Actions.CycleWS (toggleWS, nextWS, prevWS, shiftToNext, shiftToPrev )
import XMonad.Actions.WindowGo (runOrRaise, raiseMaybe)
import XMonad.Actions.RotSlaves (rotAllDown, rotAllUp, rotSlavesUp, rotSlavesDown)
import XMonad.Actions.CopyWindow (copy, copyToAll, killAllOtherCopies, kill1)
import XMonad.Actions.WithAll (killAll, sinkAll)
import XMonad.Actions.FindEmptyWorkspace (viewEmptyWorkspace, tagToEmptyWorkspace)

-- Utilities
import XMonad.Util.Run(spawnPipe, runInTerm)
import XMonad.Util.EZConfig (additionalKeysP, removeKeysP)
import XMonad.Util.NamedScratchpad

-- Layouts
import XMonad.Layout.LayoutCombinators ((|||), JumpToLayout(..))
import XMonad.Layout.NoBorders (smartBorders)     -- smart borders on solo clients
import XMonad.Layout.Fullscreen (fullscreenFull)
import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.Named
import XMonad.Layout.Tabbed
import XMonad.Layout.TwoPane

-- Prompts
import XMonad.Prompt
import XMonad.Prompt.Shell (prompt, shellPrompt)
import XMonad.Prompt.Window
import XMonad.Prompt.AppendFile


-- Misc
import XMonad.StackSet as W (focusUp, focusDown, swapDown, swapUp, shift, view, RationalRect(..))
import Graphics.X11.Xlib.Extras -- make floating menu and dialog windows
import Foreign.C.Types (CLong) -- make floating menu and dialog windows




myWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9"] ++ ["NSP"]
myTerm = "terminator"




myStartup :: X ()
myStartup = do
        spawn "/home/axel/.xmonad/autostart.sh"




-- (W.RationalRect l t w h) where h=height, w=width, t=distance from top edge, l=distance from left edge
myScratchpads =
        [ NS "term" "terminator --title=term" (title =? "term") (customFloating $ W.RationalRect 0.05 0.1 0.9 0.8)
        , NS "htop" "terminator --role=htop -e htop" (role =? "htop") (customFloating $ W.RationalRect 0.05 0.25 0.9 0.5)
        , NS "news" "terminator --role=news -e newsbeuter" (role =? "news") (customFloating $ W.RationalRect 0.15 0.05 0.7 0.90)
        ] where role = stringProperty "WM_WINDOW_ROLE"




mylayout =    onWorkspace "1" ( avoidStruts $ tiles ||| twopanes ||| mirrors ||| detailed )
            $ onWorkspace "2" ( avoidStruts $ tabs )
            $ onWorkspace "4" ( avoidStruts $ tabs ||| mirrors )
            $ avoidStruts ( tiles ||| mirrors ||| tabs ||| detailed ) ||| fills
                       
                where
                    tall      = Tall 1 (3/100) (1/2)
                    tiles     = named "tall" tall
                    twopanes  = named "2pane" (TwoPane (3/100) (1/2))
                    mirrors   = named "mirror" (Mirror tall)
                    tabs      = named "tabb" (tabbed shrinkText myTabb)
                    detailed  = named "full" Full
                    fills     = named "filled" (fullscreenFull Full)




-- To find the property name associated with a program, use (second value)
-- > xprop | grep WM_CLASS
-- > WM_CLASS(STRING) = "emacs", "Emacs23"
--   where "emacs" is resource (appName), "Emacs23" is className
-- > xprop | grep WM_NAME   -- this is Title
myManageHook = composeAll
            [ className =? "Geany"            --> doShift "2"
            , className =? "Gvim"             --> doShift "2"
            , className =? "Google-chrome"    --> doShift "3"
            , className =? "Firefox"          --> doShift "3"
            , className =? "Tkabber.tcl"      --> doShift "4"
            , className =? "Thunar"           --> doShift "5"
            , className =? "Deadbeef"         --> doShift "5"
            , className =? "Transmission-gtk" --> doShift "6"
            
            -- ignore
            , resource  =? "XXkb"             --> doIgnore
            , className =? "trayer"           --> doIgnore
            , resource  =? "xfce4-notifyd"    --> doIgnore
            
            -- floats
            , className =? "Keepassx"         --> doFloat
            , className =? "Galculator"       --> doCenterFloat
            , isDialog                        --> doCenterFloat
            , isFullscreen --> (doF W.focusDown <+> doFullFloat) -- flash player fullscreen mode
        
            -- Firefox windows... all float
            , ( className =? "Firefox" <&&> resource =? "Dialog")       --> doCenterFloat
            , ( className =? "Firefox" <&&> resource =? "Extension")    --> doCenterFloat
            , ( className =? "Firefox" <&&> resource =? "Browser")      --> doCenterFloat
            , ( className =? "Firefox" <&&> resource =? "Places")       --> doCenterFloat
            , ( className =? "Firefox" <&&> resource =? "Download")     --> doCenterFloat
            , ( className =? "Firefox" <&&> resource =? "Abp")          --> doCenterFloat
            , ( className =? "Firefox" <&&> resource =? "StylishEdit*") --> doCenterFloat
            ]




bindKeys =
        -- apps
        [ ("S-<Print>", spawn "xfce4-screenshooter")
        , ("C-<Print>", spawn "sleep 0.2; scrot -s '%Y-%m-%d--%s_$wx$h_scrot.png' -e 'mv $f ~/images/ &amp; viewnior ~/images/$f'")
        , ("<Print>", spawn "sleep 2.0; scrot '%Y-%m-%d--%s_$wx$h_scrot.png' -e 'mv $f ~/images/ &amp; viewnior ~/images/$f'")
        , ("M-e", runOrRaise "gvim" (className =? "Gvim"))
        , ("M-S-e", runOrRaise "geany" (className =? "Geany"))
        , ("M-t", runOrRaise "transmission-gtk" (className =? "Transmission-gtk"))
        , ("M-r", runOrRaise "/home/axel/.xmonad/tkabber.sh" (className =? "Tkabber.tcl"))
        , ("M-d", runOrRaise "/home/axel/.xmonad/deadbeef.sh" (className =? "Deadbeef"))
        , ("M-w", runOrRaise "/usr/bin/firefox" (className =? "Firefox"))
        , ("M-f", runOrRaise "thunar" (className =? "Thunar"))
        , ("M-<Return>", spawn myTerm)
        , ("M-i", spawn "galculator")
                
        -- client
        , ("M-<Up>", windows W.focusUp)
        , ("M-<Down>", windows W.focusDown)
        , ("M-=", sinkAll) -- un-float all floating windows on the current workspace
        , ("M-,", rotAllUp) -- rotate all the windows in the current stack
        , ("M-.", rotAllDown) -- rotate all the windows in the current stack
        , ("M-[", rotSlavesUp) -- rotate all windows except the master window
        , ("M-]", rotSlavesDown) -- rotate all windows except the master window
        , ("M1-<Tab>", toggleWS) -- toggle last workspace (super-tab)
        , ("M-<Right>", nextWS) -- go to next workspace
        , ("M-<Left>", prevWS) -- go to prev workspace
        , ("M-M1-<Right>", shiftToNext) -- move client to next workspace
        , ("M-M1-<Left>" , shiftToPrev) -- move client to prev workspace
        , ("M-<Escape>", kill1)
        , ("M-S-<Escape>", killAll)
        , ("M-C-<Escape>", spawn "xkill")
        , ("M-c", windows copyToAll)
        , ("M-S-c", killAllOtherCopies)
        
        -- actions
        , ("M-<F10>", viewEmptyWorkspace) -- find and view an empty workspace
        , ("M-S-<F10>", tagToEmptyWorkspace) -- tag current window to an empty workspace and view it
        
        -- layout
        , ("M-<F1>", sendMessage $ JumpToLayout "tall")
        , ("M-<F2>", sendMessage $ JumpToLayout "mirror")
        , ("M-<F3>", sendMessage $ JumpToLayout "tabs")
        , ("M-<F4>", sendMessage $ JumpToLayout "full")
        , ("M-<F5>", sendMessage $ JumpToLayout "filled")
        
        -- system
        , ("M-<F12>", spawn "cb-exit") -- Crunachbang Linux "Exit" utilite
        , ("M-<F11>", spawn "xscreensaver-command -activate")
        , ("M-S-<F11>", spawn "xscreensaver-command -lock")
                
        -- scratchpad
        , ("M-x", namedScratchpadAction myScratchpads "term")
        , ("M-a", namedScratchpadAction myScratchpads "htop")
        , ("M-s", namedScratchpadAction myScratchpads "news")
        
        
        -- prompt
        , ("M-S-p", prompt (myTerm ++ " -e") myPrompt) -- run application in terminal
        , ("M-p", shellPrompt myPrompt) -- run
        , ("M-g", windowPromptGoto myPrompt) -- window go prompt
        , ("M-b", windowPromptBring myPrompt) -- window bring promp
        , ("M-S-b", windowPromptBringCopy myPrompt) -- window bring copy promp

        , ("M-n", do
                spawn ("date>>"++"/home/axel/.NOTES")
                appendFilePrompt myPrompt "/home/axel/.NOTES") 
        ]
                
        -- misc
        ++
        -- "M-[1..9]" -- Switch to workspace N
        -- "M-S-[1..9]" -- Move client to workspace N
        -- "M-C-[1..9]" -- Copy client to workspace N
        [("M-" ++ m ++ [k], windows $ f i)
            | (i, k) <- zip myWorkspaces ['1' .. '9']
            , (f, m) <- [ (W.view, "")
                        , (W.shift, "S-")
                        , (copy, "C-")
                        ]
        ]
        ++
        -- "M-Alt-[1..9]" -- Move client to workspace N and follow
        [("M-M1-" ++ [k], (windows $ W.shift i) >> (windows $ W.view i))
            | (i, k) <- zip myWorkspaces ['1' .. '9']
        ]
        
        


unbindKeys = 
        [ ("M-m")
        , ("M-S-<Return>")
        ]




-- make floating menu and dialog windows
getProp :: Atom -> Window -> X (Maybe [CLong])
getProp a w = withDisplay $ \dpy -> io $ getWindowProperty32 dpy a w

checkAtom name value = ask >>= \w -> liftX $ do
          a <- getAtom name
          val <- getAtom value
          mbr <- getProp a w
          case mbr of
            Just [r] -> return $ elem (fromIntegral r) [val]
            _ -> return False

checkDialog = checkAtom "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_DIALOG"
checkMenu = checkAtom "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_MENU"

manageMenus = checkMenu --> doFloat
manageDialogs = checkDialog --> doFloat




main = do
        xmproc <- spawnPipe "/home/axel/.cabal/bin/xmobar /home/axel/.xmonad/xmobarrc"
        xmonad $ defaults
            { manageHook = manageDocks <+> myManageHook <+> manageMenus <+> manageDialogs <+> namedScratchpadManageHook myScratchpads 
            , logHook = dynamicLogWithPP $ xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppHidden = filterScratchPad
                        , ppCurrent = xmobarColor "#ee9a00" "" . wrap "[" "]"
--                        , ppHiddenNoWindows = xmobarColor "#777777" "" 
                        , ppTitle = xmobarColor "lightgreen" "" . shorten 80
                        , ppSep = " | "
--                        , ppSort   = fmap (namedScratchpadFilterOutWorkspace.) (ppSort xmobarPP) -- hide 'NSP' from the workspace list --
                        }
            } `additionalKeysP` bindKeys `removeKeysP` unbindKeys
            where
                  filterScratchPad ws = if ws == "NSP" then "S" else ws
                  




defaults = defaultConfig {
          terminal           = myTerm
        , workspaces         = myWorkspaces
        , layoutHook         = smartBorders $ mylayout
        , normalBorderColor  = "#888888"
        , focusedBorderColor = "#11A2F0"
        , borderWidth        = 1
        , modMask            = mod4Mask
        , focusFollowsMouse  = False
        , startupHook        = myStartup
        }

--- ( #00F353 - green ), ( #11A2F0 - blue )

-- shell prompt theme
myPrompt = defaultXPConfig
         { font              = "xft:Liberation Mono-9:regular"
         , bgColor           = "#000000"
         , fgColor           = "#8DB456"
         , fgHLight          = "#8DB456"
         , bgHLight          = "#1B1B1B"
         , borderColor       = "#8DB456"
         , promptBorderWidth = 1
         , position          = Bottom
         , height            = 20
         , historySize       = 10
         , defaultText       = []
--         , autoComplete      = Just 1000
         }

-- Tabbed theme
myTabb = defaultTheme
          { fontName            = "xft:Liberation Mono-9:Bold"
          , decoHeight          = 18
          , decoWidth           = 400
          , activeColor         = "#0A0A0A"
          , inactiveColor       = "#1B1B1B"
          , urgentColor         = "#000000"
          , activeBorderColor   = "#8DB456"
          , inactiveBorderColor = "#8DB456"
          , urgentBorderColor   = "#E84F4F"
          , activeTextColor     = "#8DB456"
          , inactiveTextColor   = "#ABABAB"
          , urgentTextColor     = "#E84F4F"
          }

